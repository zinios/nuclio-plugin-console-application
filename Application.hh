<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\console\application
{
	use nuclio\Nuclio;
	use nuclio\kernel\CLIKernel;
	use nuclio\exception\ExceptionBootstrapper;
	use nuclio\core\
	{
		loader\ClassLoader,
		ClassManager,
		EventManager,
		plugin\Plugin
	};
	use nuclio\plugin\console\
	{
		ArgumentDef,
		Console,
		Command,
		ConsoleException
	};
	use nuclio\plugin\config\Config;
	use nuclio\plugin\format\reader\Reader as FileReader;
	
	use nuclio\helper\CryptHelper;
	use \ReflectionClass;
	
	abstract class Application extends Plugin
	{
		private ?CLIKernel $kernel	=null;
		private ?Nuclio $nuclio	=null;
		private Console $console;
		public CryptHelper $cryptHelper;
		
		public function __construct()
		{
			parent::__construct();
			// var_dump('after parent construct'); die();			
			try
			{
				$this->kernel=new CLIKernel
				(
					shape
					(
						'classLoader'		=>ClassLoader::getInstance(),
						'classManager'		=>ClassManager::getInstance(),
						'eventManager'		=>EventManager::getInstance(),
						'exceptionHandler'	=>new ExceptionBootstrapper('cli')
					)
				);
				$this->nuclio=new Nuclio($this->kernel);
			}
			catch (\Exception $exception)
			{
				var_dump($exception);
				exit();
			}
			
			//Init Console.
			$this->console	=Console::getInstance();
			
			$reflection		=new ReflectionClass($this);
			$command		=null;
			$arguments		=new Map(null);
			
			if (method_exists($this,'onMapCommands'))
			{
				$this->onMapCommands();
			}
			
			foreach ($reflection->getMethods() as $method)
			{
				$attributes=new Map($method->getAttributes());
				if (!$attributes->count())
				{
					continue;
				}
				if ($attributes->containsKey('command') || $attributes->containsKey('arguments'))
				{
					if (!$attributes->containsKey('command'))
					{
						throw new ConsoleException('Invalid console command definition. A command must define at least a "command" attribute.');
					}
					$commandArg=$attributes['command'][0];
					if (!is_string($commandArg))
					{
						throw new ConsoleException('Command must be a string.');
					}
					$handler=null;
					if ($attributes->containsKey('handler'))
					{
						if (count($attributes['handler'])==2)
						{
							$handler=$attributes['handler'];
						}
						else
						{
							throw new ConsoleException('Invalid console command definition. If a command refers to a handler. The command must specify a class and method to call.');
						}
					}
					$command=$this->console->addCommand
					(
						$commandArg,
						function(Command $command):void use ($method,$handler)
						{
							if (!is_null($handler))
							{
								ClassManager::getClassInstance($handler[0])
								|> call_user_func_array([$$,$handler[1]],[$command]);
							}
							else
							{
								call_user_func_array([$this,$method->name],[$command]);
							}
						}
					);
					if (!$attributes->containsKey('arguments'))
					{
						continue;
					}
					foreach ($attributes['arguments'] as $argDef)
					{
						if (!is_array($argDef))
						{
							throw new ConsoleException('Invalid console command definition. Arguments must be specified as arrays');
						}
						$argDef=new Vector($argDef);
						
						if (!$argDef->containsKey(0) || (!is_string($argDef[0]) && !is_array($argDef[0])))
						{
							throw new ConsoleException('Invalid console command definition. Argument must define a name as a string or an array of strings (array position 0).');
						}
						if ($argDef->containsKey(1) && !is_string($argDef[1]))
						{
							throw new ConsoleException('Invalid console command definition. Argument description must be a string (array position 1).');
						}
						if ($argDef->containsKey(2) && !is_bool($argDef[2]))
						{
							throw new ConsoleException('Invalid console command definition. Argument required flag must be a boolean (array position 2).');
						}
						$alias=null;
						$name=$argDef[0];
						if (is_string($argDef[0]))
						{
							$name=(string)$argDef[0];
						}
						else if ($argDef[0] instanceof Traversable)
						{
							$tmp=new Vector($argDef[0]);
							if ($tmp->containsKey(0))
							{
								$name=$tmp[0];
								$tmp->removeKey(0);
								if ($tmp->count())
								{
									$alias=$tmp;
								}
							}
							else
							{
								die('ffs');
							}
						}
						$description='';
						$required	=false;
						if ($argDef->containsKey(1))
						{
							$description=$argDef[1];
						}
						if ($argDef->containsKey(2))
						{
							$required=$argDef[2];
						}
						$command->addArgument
						(
							shape
							(
								'name'			=>$name,
								'alias'			=>$alias,
								'description'	=>$description,
								'required'		=>$required,
								'values'		=>null
							)
						);
					}
				}
			}
			if (!method_exists($this,'init'))
			{
				$this->console->execute();
			}
			else
			{
				$this->init();
			}
		}
		
		public function getConsole():Console
		{
			return $this->console;
		}
		
		public function getDispatcher()
		{
			return $this->kernel->getEventManager()->getDispatcher();
		}
		
		public function loadCommandsFromFile(string $fileName)
		{
			$rawCommands=FileReader::getContent($fileName);
			
			$rawCommands=$rawCommands->get('commands');
			if (is_null($rawCommands))
			{
				throw new ConsoleException('Invalid console config. Expected "commands" section.');
			}
			$rawCommands=new Map($rawCommands);
			foreach ($rawCommands as $commandName=>$commandConfig)
			{
				$commandConfig=new Map($commandConfig);
				$handler=null;
				if (!$commandConfig->containsKey('handler'))
				{
					throw new ConsoleException('Invalid console command definition. The command must specify a class and method to call.');
				}
				$command=$this->console->addCommand
				(
					$commandName,
					function(Command $command):void use ($commandConfig)
					{
						$handler=new Map($commandConfig->get('handler'));
						if (!$handler->containsKey('class') || !$handler->containsKey('method'))
						{
							throw new ConsoleException('Invalid console command definition. The command must specify a handler with a reference to the class and method.');
						}
						ClassManager::getClassInstance($handler->get('class'))
						|> call_user_func_array([$$,$handler->get('method')],[$command]);
					}
				);
				
				if ($commandConfig->containsKey('arguments'))
				{
					foreach ($commandConfig->get('arguments') as $argName=>$argDef)
					{
						if (!is_array($argDef))
						{
							throw new ConsoleException('Invalid console command definition. Arguments must be specified as arrays');
						}
						$argDef=new Map($argDef);
						
						if ($argDef->containsKey('required') && !is_bool($argDef->get('required')))
						{
							throw new ConsoleException('Invalid console command definition. Argument required flag must be a boolean.');
						}
						$alias=null;
						if ($argDef->containsKey('alias'))
						{
							$alias=$argDef->get('alias');
						}
						$command->addArgument
						(
							shape
							(
								'name'			=>$argName,
								'alias'			=>$alias,
								'description'	=>$argDef->get('description') ?? '',
								'required'		=>$argDef->get('required') ?? false,
								'values'		=>null
							)
						);
					}
				}
			}
		}
	}
}
